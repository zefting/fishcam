#from picamera import PiCamera
from datetime import date, timedelta, time, datetime
from time import sleep
from astral import Astral
import csv
# coding: latin-1

#from w1thermsensor import W1ThermSensor
#import schedule
tidnu = datetime.now()
#https://astral.readthedocs.io/en/latest/
city_name = 'Copenhagen'
a = Astral()
a.solar_depression = 'civil'
city = a[city_name]
sun = city.sun(date=date(tidnu.year, tidnu.month, tidnu.day), local=True)
moon_phase = a.moon_phase(date=date(tidnu.year, tidnu.month, tidnu.day))

tidned= sun['sunset']
tidnedSol = tidned.strftime("%H,%M")
print(tidnedSol)
tidop = sun['sunrise']
tidopSol = tidop.strftime("%H,%M")
print(tidopSol)
#https://stackoverflow.com/questions/10048249/how-do-i-determine-if-current-time-is-within-a-specified-range-using-pythons-da
def is_time_between(begin_time, end_time, check_time=None):
    # If check time is not given, default to current UTC time
    check_time = check_time or datetime.utcnow().time()
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else: # crosses midnight
        return check_time >= begin_time or check_time <= end_time

        #konverter maanefaserne fra tal til ord. https://astral.readthedocs.io/en/latest/
def Maanefase(moon_phase):
    if moon_phase < 6:
        return 'Nymaane',  " Vaerdi:", moon_phase
    elif moon_phase <= 7:
        return 'Tiltagende maane',  " Vaerdi:", +moon_phase
    elif moon_phase <= 14:
        return 'Fuldmaane',  " Vaerdi:", moon_phase
    elif moon_phase <= 21:
        return 'Aftagende maane'  " Vaerdi:", moon_phase
    [moon_phase]



#https://www.raspberrypi.org/documentation/raspbian/applications/camera.md
#def startPictureLoop():



#    tempnu = schedule.every(1).hour.do(tempnu(sensor))
    #start csv-skriver
    #if is_time_between(time(sun['sunrise']), time(sun['sunset'])):

camera = PiCamera()
camera.resolution = (1024, 768)

#camera.EXIF = 'ExposureMode'
camera.exposure_mode = 'auto'
camera.exif_tags['EXIF.UserComment'] = str(Maanefase (moon_phase)) + '0 = nymaane 7 = tiltagende 14 = Fuldmaane 21 = aftagende'
while True is_time_between (time(tidopSol), time(tidnedSol)):
    date = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
    sti = "/home/pi/fishcam/billeder/" + date + ".jpg"
    camera.capture(sti)
    with open('/home/pi/fishcam/log/camlog.csv', 'a') as csvfile:
        camwriter = csv.writer(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        camwriter.writerow([date, Maanefase(moon_phase)])

sleep(5)

else:
    camera.exposure_mode = 'night'
    camera.exif_tags['EXIF.UserComment'] = Maanefase (moon_phase), '0 = nymaane 7 = tiltagende 14 = Fuldmaane 21 = aftagende'
    camera.capture("/home/pi/fishcam/billeder/"+ date + '.jpg')
    with open('/home/pi/fishcam/log/camlog.csv', 'w', newline='') as csvfile:
    camwriter = csv.writer(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    camwriter.writerow([camdate, Maanefase(moon_phase), camera.EXIF])
    sleep(5)
