import datetime
from datetime import date, timedelta, time
import time
from astral import Astral
import csv


tidnu = datetime.datetime.now()
#https://astral.readthedocs.io/en/latest/
city_name = 'Copenhagen'
a = Astral()
a.solar_depression = 'civil'
city = a[city_name]
#sun = city.sun(date=datetime.date(tidnu.year, tidnu.month, tidnu.day), local=True)
#moon_phase = a.moon_phase(date=datetime.date(tidnu.year, tidnu.month, tidnu.day))


def daterange(date1, date2):
    for n in range(int ((date2 - date1).days)+1):
        yield  date1 + timedelta(n)
start_dt = date(2019, 7, 4)
end_dt = date(2019, 12, 30)
with open('dato/dato.csv', 'w', newline='') as csvfile:
    for dt in daterange(start_dt, end_dt):
        camwriter = csv.writer(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        sun = city.sun(dt)
        moon_phase = a.moon_phase(dt)
        camwriter.writerow([dt, sun['sunrise'], sun['sunset'], [moon_phase]])
