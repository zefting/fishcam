#!/usr/bin/python3
import pexpect
import os
from datetime import datetime
startTime = datetime.now()
print(startTime)
print("import begyndt")
child = pexpect.spawn('/bin/bash', ['-c',  'rsync -av --remove-source-files --progress :/www/fishcam/billeder'], encoding='utf-8', timeout=None)
child.logfile = open("log/importlog.txt", "a+")  # log what the child sends back

child.expect("(?i) password:")
child.sendline('')
child.expect(pexpect.EOF)

print("import faerdig")
child.logfile.close()
print(datetime.now() - startTime)
