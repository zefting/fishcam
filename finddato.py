import datetime
import time
import csv

#https://www.geeksforgeeks.org/working-csv-files-python/, https://stackoverflow.com/questions/40945759/python-and-csv-find-a-row-and-give-column-value
dato = datetime.date.today().strftime("%Y-%m-%d")

# csv file name
filename = "dato/dato.csv"
# initializing the titles and rows list

# reading csv file
with open(filename, 'r') as csvfile:
    # creating a csv reader object
    csvfund = any(dato == row[0] for row in csv.reader(csvfile,delimiter=';'))
print(csvfund)

csvfile.close()  
