import shutil
from datetime import date, timedelta, time, datetime
date = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
output_filename = "/home/pi/fishcam/" + date + ".zip"
dir_name = "/home/pi/fishcam/billeder/"
shutil.make_archive(output_filename, 'zip', dir_name)
print ("done")
